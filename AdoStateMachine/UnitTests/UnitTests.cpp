// UnitTests.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "gtest/gtest.h"
#include "Graph.h"
#include "Guid.h"
#include "Node.h"
#include "NodeConnection.h"
#include "NodeData.h"
#include "awful.hpp"

template <class NodeDataType>
class VectorNodeStoragePolicy
{
public:

    using NodeType = Node<NodeDataType>;

    template <class... Args>
    const NodeType& Emplace(Guid guid, Args&&... args)
    {
        return m_Nodes.emplace_back(guid, std::forward<Args>(args)...);
    }

    nonstd::observer_ptr<const NodeType> Find(Guid guid) const
    {
        const auto wanted_node = std::find_if(
            m_Nodes.begin(),
            m_Nodes.end(),
            [&guid](const auto& node)
            {
                return node.GetGuid() == guid;
            }
        );
		if (wanted_node != m_Nodes.end())
		{
			return nonstd::make_observer<const NodeType>(&(*wanted_node));
		}
		else
		{
			return nullptr;
		}
    }

    void Erase(Guid node_guid)
    {
        m_Nodes.erase(std::remove(m_Nodes.begin(), m_Nodes.end(), node_guid), m_Nodes.end());
    }

    template <class F>
    void ForEachNode(F f) const
    {
        for (const NodeType& node : m_Nodes)
        {
            f(node);
        }
    }

    template <class F>
    void ForEachNode(F f)
    {
        using ThisType = std::decay_t<decltype(*this)>;
        const_cast<const ThisType*>(this)->ForEachNode(f);
    }

private:

    std::vector<NodeType> m_Nodes;

};

template <bool DIRECTED>
class VectorConnectionStoragePolicy
{
public:

    static constexpr bool IS_DIRECTED = DIRECTED;

    bool Has(Guid from, Guid to) const
    {
		for (const NodeConnection& connection : m_Connections)
		{
			if ((connection.from == from && connection.to == to) ||
				(!DIRECTED  ?  (connection.from == to && connection.to == from) : false))
			{
				return true;
			}
		}

		return false;
    }

    void Emplace(Guid from, Guid to)
    {
        assert(!Has(from, to));
        m_Connections.emplace_back(from, to);
    }

    void Erase(Guid from, Guid to)
    {
		const auto new_end = std::remove_if(
			m_Connections.begin(),
			m_Connections.end(),
			[&from, &to](const auto& connection) {
			return ((connection.from == from && connection.to == to) ||
				(!DIRECTED ? (connection.from == to && connection.to == from) : false));
		});

		m_Connections.erase(new_end, m_Connections.end());
    }

    void EraseAllIncident(Guid node_guid)
    {
        const auto new_end = std::remove_if(
            m_Connections.begin(),
            m_Connections.end(),
            [&node_guid](const auto& connection) {
                return connection.from == node_guid || connection.to == node_guid;
            }
        );

        m_Connections.erase(new_end, m_Connections.end());
    }

private:

    std::vector<NodeConnection> m_Connections;

};

// 0. skonczyc implementacje z wektorem
// 1. zrobic implementacje z: flat map�, map�, unordered_map�
// - troch� jako �wiczenie i ciekawostk�, bo potem warto
// by�oby zrobi� benchmark i wyci�gn�� wnioski
// 2. rozwa�y� zrobienie interfejsu do kontenera i
// w policy u�ywa� tego� (tj. np. VectorStoragePolicy,
// kt�re implementuje s�ownik na wektorze i VectorNodePolicy
// / VectorConnectionPolicy, kt�re g��wnie deleguj�
// wywo�ania do niego) - POTENCJALNIE NIE WARTO
// 3. zarobi� miljon dolar�w na sprzeda�y tej biblioteki lub cia�a
//
// mikosz@protonmail.com

TEST(test_graph, TestInsertNodes)
{
	// TESTING INSERT NODES - START
	auto graph = Graph<VectorNodeStoragePolicy<int>, VectorConnectionStoragePolicy<true>>();
	auto first_node_guid = graph.InsertNode(1);
	auto second_node_guid = graph.InsertNode(2);
	auto third_node_guid = graph.InsertNode(3);

	// Test if inserted nodes are different.
	EXPECT_NE(first_node_guid.GetId(), second_node_guid.GetId());
	EXPECT_NE(first_node_guid.GetId(), third_node_guid.GetId());
	EXPECT_NE(second_node_guid.GetId(), third_node_guid.GetId());

	// Test if inserted nodes contain proper data
	EXPECT_EQ(graph.GetNode(first_node_guid)->GetData(), 1);
	EXPECT_EQ(graph.GetNode(second_node_guid)->GetData(), 2);
	EXPECT_EQ(graph.GetNode(third_node_guid)->GetData(), 3);
	
	EXPECT_NE(graph.GetNode(first_node_guid)->GetData(), 666);
	EXPECT_NE(graph.GetNode(second_node_guid)->GetData(), 666);
	EXPECT_NE(graph.GetNode(third_node_guid)->GetData(), 666);
	// TESTING ADDING NODES - END
}

TEST(test_graph, TestEmplaceNodes)
{
    // TESTING EMPLACE NODES - START
    auto graph = Graph<VectorNodeStoragePolicy<int>, VectorConnectionStoragePolicy<true>>();
    auto first_node_guid = graph.EmplaceNode(1);
    auto second_node_guid = graph.EmplaceNode(2);
    auto third_node_guid = graph.EmplaceNode(3);

    // Test if emplaced nodes are different.
    EXPECT_NE(first_node_guid.GetId(), second_node_guid.GetId());
    EXPECT_NE(first_node_guid.GetId(), third_node_guid.GetId());
    EXPECT_NE(second_node_guid.GetId(), third_node_guid.GetId());

    // Test if emplaced nodes contain proper data
    EXPECT_EQ(graph.GetNode(first_node_guid)->GetData(), 1);
    EXPECT_EQ(graph.GetNode(second_node_guid)->GetData(), 2);
    EXPECT_EQ(graph.GetNode(third_node_guid)->GetData(), 3);

    EXPECT_NE(graph.GetNode(first_node_guid)->GetData(), 666);
    EXPECT_NE(graph.GetNode(second_node_guid)->GetData(), 666);
    EXPECT_NE(graph.GetNode(third_node_guid)->GetData(), 666);
    // TESTING EMPLACE NODES - END
}

TEST(test_graph, TestErasingNode)
{
	// ADDING NODES - START
    auto graph = Graph<VectorNodeStoragePolicy<int>, VectorConnectionStoragePolicy<true>>();
    auto first_node_guid = graph.InsertNode(1);
	auto second_node_guid = graph.InsertNode(2);
	auto third_node_guid = graph.InsertNode(3);
	// ADDING NODES - END

	// ADD TEST CONNECTION
	graph.InsertConnection(first_node_guid, second_node_guid);
	graph.InsertConnection(second_node_guid, third_node_guid);
	graph.InsertConnection(third_node_guid, first_node_guid);

	// TESTING REMOVING NODE - START
	graph.EraseNode(first_node_guid);
	EXPECT_FALSE(graph.GetNode(first_node_guid));

	// Check if connection for node was deleted as well.
	EXPECT_FALSE(graph.HasConnection(first_node_guid, second_node_guid));
	EXPECT_TRUE(graph.HasConnection(second_node_guid, third_node_guid));
	EXPECT_FALSE(graph.HasConnection(third_node_guid, first_node_guid));
	// TESTING REMOVING NODE - END
}

TEST(test_graph, TestAddingConnections)
{
	// DIRECTED GRAPH _ START
	// ADDING NODES - START
    auto graph = Graph<VectorNodeStoragePolicy<int>, VectorConnectionStoragePolicy<true>>();
	auto first_node_guid = graph.InsertNode(1);
	auto second_node_guid = graph.InsertNode(2);
	auto third_node_guid = graph.InsertNode(3);
	// ADDING NODES - END

	// TESETING ADDING CONNECTIONS - START
	graph.InsertConnection(first_node_guid, second_node_guid);
	EXPECT_TRUE(graph.HasConnection(first_node_guid, second_node_guid));
	EXPECT_FALSE(graph.HasConnection(second_node_guid, first_node_guid));
	EXPECT_FALSE(graph.HasConnection(first_node_guid, third_node_guid));
	EXPECT_FALSE(graph.HasConnection(third_node_guid, first_node_guid));

	graph.InsertConnection(second_node_guid, third_node_guid);
	EXPECT_TRUE(graph.HasConnection(second_node_guid, third_node_guid));
	EXPECT_FALSE(graph.HasConnection(third_node_guid, second_node_guid));
	EXPECT_FALSE(graph.HasConnection(second_node_guid, first_node_guid));
	EXPECT_TRUE(graph.HasConnection(first_node_guid, second_node_guid));

	graph.InsertConnection(third_node_guid, first_node_guid);
	EXPECT_TRUE(graph.HasConnection(third_node_guid, first_node_guid));
	EXPECT_FALSE(graph.HasConnection(first_node_guid, third_node_guid));
	EXPECT_FALSE(graph.HasConnection(third_node_guid, second_node_guid));
	EXPECT_TRUE(graph.HasConnection(second_node_guid, third_node_guid));
	// TESETING ADDING CONNECTIONS - END
	// DIRECTED GRAPH - END

	// BIDIRECTIONAL GRAPH _ START
	// ADDING NODES - START
    auto bi_graph = Graph<VectorNodeStoragePolicy<int>, VectorConnectionStoragePolicy<false>>();
    first_node_guid = graph.InsertNode(1);
	second_node_guid = graph.InsertNode(2);
	third_node_guid = graph.InsertNode(3);

	// ADDING NODES - END

	// TESETING ADDING CONNECTIONS - START
	bi_graph.InsertConnection(first_node_guid, second_node_guid);
	EXPECT_TRUE(bi_graph.HasConnection(first_node_guid, second_node_guid));
	EXPECT_TRUE(bi_graph.HasConnection(second_node_guid, first_node_guid));
	EXPECT_FALSE(bi_graph.HasConnection(first_node_guid, third_node_guid));
	EXPECT_FALSE(bi_graph.HasConnection(third_node_guid, first_node_guid));

	bi_graph.InsertConnection(second_node_guid, third_node_guid);
	EXPECT_TRUE(bi_graph.HasConnection(second_node_guid, third_node_guid));
	EXPECT_TRUE(bi_graph.HasConnection(third_node_guid, second_node_guid));
	EXPECT_TRUE(bi_graph.HasConnection(second_node_guid, first_node_guid));
	EXPECT_TRUE(bi_graph.HasConnection(first_node_guid, second_node_guid));
	// TESETING ADDING CONNECTIONS - END
	// BIDIRECTIONAL GRAPH - END
}

TEST(test_graph, TestErasingConnections)
{
	// DIRECTED GRAPH _ START
	// ADDING NODES - START
    auto graph = Graph<VectorNodeStoragePolicy<int>, VectorConnectionStoragePolicy<true>>();
	auto first_node_guid = graph.InsertNode(1);
	auto second_node_guid = graph.InsertNode(2);
	auto third_node_guid = graph.InsertNode(3);
	// ADDING NODES - END

	graph.InsertConnection(first_node_guid, second_node_guid);
	graph.InsertConnection(second_node_guid, first_node_guid);
	graph.InsertConnection(second_node_guid, third_node_guid);

	// TESETING REMOVING CONNECTIONS - START
	graph.EraseConnection(first_node_guid, second_node_guid);
	EXPECT_FALSE(graph.HasConnection(first_node_guid, third_node_guid));
	EXPECT_TRUE(graph.HasConnection(second_node_guid, first_node_guid));
	EXPECT_TRUE(graph.HasConnection(second_node_guid, third_node_guid));

	// TESETING REMOVING CONNECTIONS - END
	// DIRECTED GRAPH - END

	// BIDIRECTIONAL GRAPH _ START
	// ADDING NODES - START
    auto bi_graph = Graph<VectorNodeStoragePolicy<int>, VectorConnectionStoragePolicy<false>>();
	first_node_guid = graph.InsertNode(1);
	second_node_guid = graph.InsertNode(2);
	third_node_guid = graph.InsertNode(3);
	// ADDING NODES - END
	
	bi_graph.InsertConnection(first_node_guid, second_node_guid);
    bi_graph.InsertConnection(second_node_guid, first_node_guid);
    bi_graph.InsertConnection(second_node_guid, third_node_guid);

	// TESETING REMOVING CONNECTIONS - START
	bi_graph.EraseConnection(second_node_guid, first_node_guid);
	EXPECT_FALSE(bi_graph.HasConnection(first_node_guid, second_node_guid));
	EXPECT_FALSE(bi_graph.HasConnection(second_node_guid, first_node_guid));
	EXPECT_TRUE(bi_graph.HasConnection(second_node_guid, third_node_guid));
	EXPECT_TRUE(bi_graph.HasConnection(third_node_guid, second_node_guid));

	// TESETING REMOVING CONNECTIONS - END
	// BIDIRECTIONAL GRAPH - END
}

TEST(test_graph, TestForEachNode)
{
	// DIRECTED GRAPH _ START
	// ADDING NODES - START
    auto graph = Graph<VectorNodeStoragePolicy<int>, VectorConnectionStoragePolicy<true>>();
	auto first_node_guid = graph.InsertNode(1);
	auto second_node_guid = graph.InsertNode(2);
	auto third_node_guid = graph.InsertNode(3);
	// ADDING NODES - END

	int test_data_to_get = 0;

	auto test_lambda = [&test_data_to_get](const Node<int>& node)
	{
		test_data_to_get += node.GetData();
	};

	graph.ForEachNode(test_lambda);

	EXPECT_EQ(test_data_to_get, 6);
}

#if 0
TEST(test_graph, TestForEachConnectionForNode)
{
	// DIRECTED GRAPH _ START
	// ADDING NODES - START
	auto graph = DirectedGraph<int, std::vector<int>, std::vector<NodeConnection>>();
	auto first_node_guid = graph.InsertNode(1);
	auto second_node_guid = graph.InsertNode(2);
	auto third_node_guid = graph.InsertNode(3);
	// ADDING NODES - END

	graph.InsertConnection(first_node_guid, second_node_guid);
	graph.InsertConnection(second_node_guid, third_node_guid);

	const auto number_of_connections = 2;

	int test_data_to_get = 0;
	auto test_lambda = [&test_data_to_get](const NodeConnection& connection)
	{
		test_data_to_get++;
	};

	graph.ForEachConnectionForNode(second_node_guid, test_lambda);
	EXPECT_EQ(number_of_connections, test_data_to_get);
}

// It just needs to compile.
TEST(test_graph, TestForLibAwful)
{
	auto graph = DirectedGraph<awful::noncopyable, std::vector<awful::noncopyable>, std::vector<NodeConnection>>();
	auto node_guid = graph.InsertNode(awful::noncopyable());

    //auto graph_2 = DirectedGraph<awful::tracked, std::vector<awful::tracked>, std::vector<NodeConnection>>();
    //auto node_guid_2 = graph_2.InsertNode(awful::tracked());

    //auto graph_3 = DirectedGraph<awful::trapconstructible, std::vector<awful::trapconstructible>, std::vector<NodeConnection>>();
    //auto node_guid_3 = graph_3.InsertNode(awful::trapconstructible());

    //auto graph_4 = DirectedGraph<awful::trapcomma, std::vector<awful::trapcomma>, std::vector<NodeConnection>>();
    //auto node_guid_4 = graph_4.InsertNode(awful::trapcomma());
}

#endif

//TEST(test_graph, TestForMapStorage)
//{
//    auto graph = DirectedGraph<int, std::map<int, bool>, std::map<NodeConnection, bool>>();
//    auto first_node_guid = graph.InsertNode(1);
//    auto second_node_guid = graph.InsertNode(2);
//    auto third_node_guid = graph.InsertNode(3);
//    // ADDING NODES - END
//
//    graph.InsertConnection(first_node_guid, second_node_guid);
//    graph.InsertConnection(second_node_guid, third_node_guid);
//
//    // Test GetNode
//    EXPECT_EQ(graph.GetNode(first_node_guid)->GetData(), 1);
//    EXPECT_EQ(graph.GetNode(second_node_guid)->GetData(), 2);
//    EXPECT_EQ(graph.GetNode(third_node_guid)->GetData(), 3);
//
//    // TESETING REMOVING CONNECTIONS
//    graph.EraseConnection(first_node_guid, second_node_guid);
//    EXPECT_FALSE(graph.HasConnection(first_node_guid, third_node_guid));
//    EXPECT_TRUE(graph.HasConnection(second_node_guid, third_node_guid));
//
//    // TESTING REMOVING NODE
//    graph.EraseNode(first_node_guid);
//    EXPECT_FALSE(graph.GetNode(first_node_guid));
//}

