#pragma once

#include <memory>
#include "Guid.h"

struct NodeConnection
{
	NodeConnection(Guid _from, Guid _to): from(_from), to(_to) {}
	Guid from;
	Guid to;
};

