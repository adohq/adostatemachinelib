#pragma once

// Temporarily using static int counter for all uids. Later it will be refactored into guid.
// Or not.

class Guid
{
public:
	Guid();
	int GetId() const;

private:
	int m_Guid = 0;

	static int GUID_COUNTER;
};

inline bool operator==(const Guid& lhs, const Guid& rhs) { return lhs.GetId() == rhs.GetId(); }
inline bool operator!=(const Guid& lhs, const Guid& rhs) { return !operator==(lhs, rhs); }
inline bool operator< (const Guid& lhs, const Guid& rhs) { return lhs.GetId() < rhs.GetId(); }
inline bool operator> (const Guid& lhs, const Guid& rhs) { return  operator< (rhs, lhs); }
inline bool operator<=(const Guid& lhs, const Guid& rhs) { return !operator> (lhs, rhs); }
inline bool operator>=(const Guid& lhs, const Guid& rhs) { return !operator< (lhs, rhs); }