#pragma once

#include <memory>
#include <string>
#include <iostream>
#include <vector>

// Free function.
template <class T>
void DebugPrintData(T&& data_to_print)
{
	std::cout << "Generic DebugPrintData is called" << std::endl;
	std::cin.ignore();
}

class NodeData
{
public:

	template <class T>
	NodeData(T data) : _data(std::make_shared<Model <T>>(std::move(data))) {}

	void DebugPrintData() const
	{
		_data->DebugPrintData();
	}

private:

	class Concept
	{
	public:
		virtual ~Concept() = default;
		virtual void DebugPrintData() const = 0;
	};

	template <class T>
	class Model : public Concept
	{
	public:
		Model(T t) : data(std::move(t)) {}

		void DebugPrintData() const override
		{
			::DebugPrintData(data);
		}
	private:
		T data;
	};

	std::shared_ptr<const Concept> _data;
};

//template <>
//void DebugPrintData(int i);

template <>
void DebugPrintData(const std::string& text);

