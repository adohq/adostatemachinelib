#include "Guid.h"

int Guid::GUID_COUNTER = 0;

Guid::Guid()
{
	GUID_COUNTER += 1;
	m_Guid = GUID_COUNTER;
}

int Guid::GetId() const
{
	return m_Guid;
}
