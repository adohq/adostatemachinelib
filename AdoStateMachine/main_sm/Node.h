#pragma once

#include <string>
#include <vector>
#include <memory>
#include "Guid.h"

template <typename NodeDataType>
class Node
{
public:

    using DataType = NodeDataType;

	Node() = delete;
	Node(Guid guid, NodeDataType&& t);

	Guid GetGuid() const;
	NodeDataType GetData() const;

private:
	Guid m_Guid;
	NodeDataType m_NodeData;
};

template <typename NodeDataType>
Node<NodeDataType>::Node(Guid guid, NodeDataType&& t)
	: m_Guid(guid)
	, m_NodeData(std::move(t))
{
}

template <typename T>
Guid Node<T>::GetGuid() const
{
	return m_Guid;
}

template<typename T>
T Node<T>::GetData() const
{
	return m_NodeData;
}

// Overloaded operators for easier comparisons for algorithms from <algorithm>
template <typename T>
inline bool operator==(const Node<T>& lhs, const Node<T>& rhs) { return lhs.GetGuid() == rhs.GetGuid(); }
template <typename T>
inline bool operator!=(const Node<T>& lhs, const Node<T>& rhs) { return !operator==(lhs, rhs); }
template <typename T>
inline bool operator< (const Node<T>& lhs, const Node<T>& rhs) { return lhs.GetGuid() < rhs.GetGuid(); }
template <typename T>
inline bool operator> (const Node<T>& lhs, const Node<T>& rhs) { return  operator< (rhs, lhs); }
template <typename T>
inline bool operator<=(const Node<T>& lhs, const Node<T>& rhs) { return !operator> (lhs, rhs); }
template <typename T>
inline bool operator>=(const Node<T>& lhs, const Node<T>& rhs) { return !operator< (lhs, rhs); }

template <typename T>
inline bool operator==(const Guid& lhs, const Node<T>& rhs) { return lhs == rhs.GetGuid(); }
template <typename T>
inline bool operator!=(const Guid& lhs, const Node<T>& rhs) { return !operator==(lhs, rhs); }
template <typename T>
inline bool operator< (const Guid& lhs, const Node<T>& rhs) { return lhs < rhs.GetGuid(); }
template <typename T>
inline bool operator< (const Node<T>& lhs, const Guid& rhs) { return lhs.GetGuid() < rhs; }
template <typename T>
inline bool operator> (const Guid& lhs, const Node<T>& rhs) { return  operator< (rhs, lhs); }
template <typename T>
inline bool operator> (const Node<T>& lhs, const Guid& rhs) { return  operator< (rhs, lhs); }
template <typename T>
inline bool operator<=(const Guid& lhs, const Node<T>& rhs) { return !operator> (lhs, rhs); }
template <typename T>
inline bool operator>=(const Guid& lhs, const Node<T>& rhs) { return !operator< (lhs, rhs); }

template <typename T>
inline bool operator==(const Node<T>& lhs, const Guid& rhs) { return lhs.GetGuid() == rhs; }
template <typename T>
inline bool operator!=(const Node<T>& lhs, const Guid& rhs) { return !operator==(lhs, rhs); }
template <typename T>
inline bool operator<=(const Node<T>& lhs, const Guid& rhs) { return !operator> (lhs, rhs); }
template <typename T>
inline bool operator>=(const Node<T>& lhs, const Guid& rhs) { return !operator< (lhs, rhs); }