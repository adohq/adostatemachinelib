#pragma once

#include <vector>
#include <map>
#include <memory>
#include <optional>
#include "Guid.h"
#include "NodeConnection.h"
#include "observer_ptr.hpp"

/// Storage policy:
/// storage needs to implement:
/// - emplace back
/// - erase
/// - begin
/// - end
/// - back
/// In order to make std algorithms work
template <class ContainedDataT>
class StoragePolicy 
{
public:
    using ContainedData = ContainedDataT;

	template <class... Args>
	void emplace_back(Args&&... args)
	{
		storage.emplace_back(std::forward<Args>(args)...);
	}

    decltype(auto) begin() const
    {
        return storage.begin();
    }

    decltype(auto) end() const
    {
        return storage.end();
    }
    decltype(auto) begin()
    {
        return storage.begin();
    }

    decltype(auto) end()
    {
        return storage.end();
    }

    decltype(auto) back()
    {
        return storage.back();
    }
    
    decltype(auto) back() const
    {
        return storage.back();
    }

    template <class iterator_type>
    decltype(auto) erase(iterator_type begin, iterator_type end)
    {
        return storage.erase(begin, end);
    }
private:
    ContainedData storage;
};

//template <std::map<class K, class V>>
//class StoragePolicy
//{
//public:
//    template <class... Args>
//    void emplace_back(Args&&... args)
//    {
//        storage.emplace(std::forward<Args>(args)...);
//    }
//
//    decltype(auto) back()
//    {
//        return storage.rbegin();
//    }
//
//    decltype(auto) back() const
//    {
//        return storage.rbegin();
//    }
//
//};

template<typename T>
class Node;

// napisz test, kt�ry sprawdzi graf z:
// std::vector (nieuporz�dkowany)
// std::map
// std::unordered_map
// implementacja r�czna gdzie std::vector jest uporz�dkowany wzgl�dem klucza <- tj taka,
// �e je�li wstawisz elementy 3, 5, 1, 2, to dostaniesz <1, 2, 3, 5>
// lower_bound <-
// upper_bound
// search (?) sprawd�

// chyba potrzeba napisa� dwa storage policy - dla node�w i dla connections

// za��my, �e chcesz u�y� storage policy dla node�w:
// class AllEvenNodes {
// public:
//
//   bool HasElement(int key) const {
//      return key % 2 == 0;
//   }
//
//   ...
// };

// AllEvenNodes nie nadaje si� jako kontener dla Connection�w (d'uh), a je�li b�dziesz
// u�ywa� tego samego storage policy dla node�w i connections, to nie b�dziesz poda� innych

template <typename NodeStoragePolicy, typename ConnectionStoragePolicy>
class Graph
{
public:

    using NodeType = typename NodeStoragePolicy::NodeType;
    using NodeDataType = typename NodeType::DataType;

	Guid InsertNode(NodeDataType&& t)
	{
		const auto& inserted_node = m_Nodes.Emplace(Guid(), std::move(t));
		return inserted_node.GetGuid();
	}

    template <class... Args>
    Guid EmplaceNode(Args&&... args)
    {
        const auto& inserted_node = m_Nodes.Emplace(Guid(), std::forward<Args>(args)...);
        return inserted_node.GetGuid();
    }

    void EraseNode(Guid node_guid)
    {
        m_Connections.EraseAllIncident(node_guid);
        m_Nodes.Erase(node_guid);
    }

	template <class F>
	void ForEachNode(F f) const
	{
        m_Nodes.ForEachNode(f);
	}

	template <class F>
	void ForEachNode(F f)
	{
        m_Nodes.ForEachNode(f);
    }

	void InsertConnection(Guid from, Guid to)
	{
        if (!m_Connections.Has(from, to))
        {
		    m_Connections.Emplace(from, to);
        }
	}
	
	void EraseConnection(Guid from, Guid to)
	{
        m_Connections.Erase(from, to);
	}

	bool HasConnection(Guid from, Guid to) const
	{
        return m_Connections.Has(from, to);
	}

	template <class F>
	void ForEachConnectionForNode(Guid node, F f) const
	{
		for (const auto& connection : m_Connections) 
		{
			if (connection.from == node || connection.to == node)
			{
				f(connection);
			}
		}
	}

	template <class F>
	void ForEachConnectionForNode(Guid node, F f) 
	{
		const_cast<const Graph*>(this)->ForEachConnectionForNode(node, f);
	}

	std::vector<NodeType>& GetNodes() const
	{
		return m_Nodes;
	}

	std::vector<NodeType>& GetNodes()
	{
		return const_cast<std::vector<NodeType>&>(const_cast<Graph*>(this)->GetNodes());
	}

	nonstd::observer_ptr<const NodeType> GetNode(Guid guid) const
	{
		return m_Nodes.Find(guid);
	}

	nonstd::observer_ptr<NodeType> GetNode(Guid guid)
	{
		return nonstd::make_observer<NodeType>(const_cast<NodeType*>(const_cast<const Graph*>(this)->GetNode(guid).get()));
	}

private:
    NodeStoragePolicy m_Nodes;
    ConnectionStoragePolicy m_Connections;

    //std::vector<NodeType> m_Nodes;
    //std::vector<NodeConnection> m_Connections;
};
