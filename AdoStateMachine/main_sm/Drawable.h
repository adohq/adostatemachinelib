#pragma once

#include <memory>
#include <string>
#include <iostream>
#include <vector>

// Free function.
template <class T>
void Draw(T&& data_to_draw)
{
	cout << "Generic Draw is called" <<endl;
	std::cin.ignore();
}

class Drawable
{
public:
	template <class T>
	Drawable(T data) : _data(std::make_shared<Model <T>>(std::move(data))) {}

	void Draw() const
	{
		_data->Draw();
	}

private:
	class Concept 
	{
	public:
		virtual ~Concept() = default;
		virtual void Draw() const = 0;
	};

	template <class T>
	class Model : public Concept
	{
		Model(T t) : data(std::forward(t)) {}

		void Draw() const override
		{
			Draw(data_to_draw);
		}

		T data;
	};

	std::shared_ptr<const Concept> _data;
};

void Draw(const int& i)
{
	std::cout << "i = " << i << std::endl;
}

void Draw(const std::string& text)
{
	std::cout << "text = " << text << std::endl;
}

// For future usage: commented right now.
// To draw state_box
//void Draw(int x, int y, int size, int box_name, std::string text_inside_box)
//{
//	// dear imgui magic.
//}
